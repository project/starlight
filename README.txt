Starlight

** Theme Settings **


** Social Media Links **

To create a Social Media links bar, simply create a menu called "Social Links". It MUST have the machine name social_links. The links in this menu will automatically be replaced with social 
media icons. The following services are supported:

Facebook
Twitter
YouTube
LinkedIn


The CSS will be looking for the relevant domain names in the link href attribute, like this:

.social-links-menu a[href*='socialsite']

Please file an issue to request different services more
