(function ($) {
 // Store our function as a property of Drupal.behaviors.
 Drupal.behaviors.starlight_global = {
    attach: function (context, settings) {

    function slideMenu(e, $this) {
      e.preventDefault();
      $this.toggleClass('toggled').next('.menu', context).once().slideToggle();
    }

    // Menu dropdowns
    // Start with keyboard/touch behaviours
    $('.menu-has-dropdowns .menu-trigger', context).on('touchstart', function(e){
      slideMenu(e, $(this));
    });

    // Keyboard behaviour
    $('.menu-has-dropdowns .menu-trigger', context).on('keyup', function(e){
      if (e.which == 13) {
        console.log('slide');
        slideMenu(e, $(this));
      }
    });
    // Mouse behaviour
    $('.menu-has-dropdowns li.expanded', context).hoverIntent(function() {
       $(this).children('ul').delay(300).slideToggle();
    });

    // Split the text in the new comments link
    // This link is rendered by JS in the comment module
    var newCommentString = Drupal.t('new');

    // Rewriting text for new comment link, and moving to comments-comments list element
    // Intent is to make links less verbose and more likely to fit at different screen sizes
    $('.comment-new-comments', context).each(function(index, element) {
      var new_text = $(this).children('a').text();
      var new_no = new_text.replace(/[^0-9]/g,'');
      var new_link = $(this).children('a').attr('href');
      $(this).hide();
      if (new_no) {
        $(this).siblings('.comment-comments').append('(<a class="new-comments" href="' + new_link + '">' + new_no + ' ' + newCommentString + '</a>)')
      }
    });


  }
 }
}(jQuery));
