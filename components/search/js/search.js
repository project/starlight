(function ($) {
 // Store our function as a property of Drupal.behaviors.
 Drupal.behaviors.starlight_search = {
    attach: function (context, settings) {

    searchBlockEl = $('.site-header .block[data-drupal-selector=search-block-form]');
    menu_breakpoint = drupalSettings.starlight.mobile_menus.mobile_menu_breakpoint;
    search_collapse =  drupalSettings.starlight.search_collapse;

    // If it's mobile or the search collapse is on
    if (($(window).width() < menu_breakpoint) || (search_collapse == 1)){
      buildSearchToggle(context);
    }

    if (($(window).width() >= menu_breakpoint) && (search_collapse == 1)){
      $('.site-header').addClass('search-toggle-desktop')
    }

    // Set up the search toggle if it doesn't already exist

    function buildSearchToggle() {
      if (($('.search_toggle').length == 0 ) && context == document) {
        $(searchBlockEl, context).prepend('<button class="search-toggle"><svg version="1.1" width="2.5rem" height="2.5rem" class="header-fg-fill" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"><path d="M344.5,298c15-23.6,23.8-51.6,23.8-81.7c0-84.1-68.1-152.3-152.1-152.3C132.1,64,64,132.2,64,216.3 c0,84.1,68.1,152.3,152.1,152.3c30.5,0,58.9-9,82.7-24.4l6.9-4.8L414.3,448l33.7-34.3L339.5,305.1L344.5,298z M301.4,131.2 c22.7,22.7,35.2,52.9,35.2,85c0,32.1-12.5,62.3-35.2,85c-22.7,22.7-52.9,35.2-85,35.2c-32.1,0-62.3-12.5-85-35.2 c-22.7-22.7-35.2-52.9-35.2-85c0-32.1,12.5-62.3,35.2-85c22.7-22.7,52.9-35.2,85-35.2C248.5,96,278.7,108.5,301.4,131.2z"/></svg></button>');
        $('.site-header').addClass('search-toggle-active');
        $('.search-form', searchBlockEl).hide();
      }
    }
    // Click behaviour
    $('.search-toggle', context).click(function() {
      $('.search-form', searchBlockEl).slideToggle('1000').toggleClass('search-open');
      $('.menu-open').slideToggle('slow').removeClass('menu-open');
    });

    // Destroy the search toggle elements
    function destroySearchToggle() {
      $('.search-toggle').remove();
      $('body').removeClass('search-toggle-active');
      $('.search-form', searchBlockEl).show();
    }

    // Fix mobile/desktop behaviours on window resize
    // Set a timeout to make sure they don't keep firing continuously while the window
    // is being resized
    var resizeId;
    $(window).on('resize', function() {
      clearTimeout(resizeId);
      resizeId = setTimeout(doneResizing, 500);
    });

    // Adjustments to make on window resize
    function doneResizing() {
      // If the window goes smaller than the menu breakpoint, and it's not already built:
      if (($(window).width() < menu_breakpoint) && ($('.search-toggle').length == 0)) {
        buildSearchToggle(context);
      }
      // Or if it goes bigger than the menu breakpoint, and it's not set to collapse on larger viewports
      else if (($(window).width() >= menu_breakpoint) && (search_collapse !=  1)) {
        destroySearchToggle()
      }

      // not sure if this will work...
      if (($(window).width() < menu_breakpoint) && (search_collapse == 1)) {
        $('.site-header').removeClass('search-toggle-desktop')
      }
      else if (($(window).width() >= menu_breakpoint) && (search_collapse == 1)) {
        $('.site-header').addClass('search-toggle-desktop')
      }

    }

  }
 }
}(jQuery));
