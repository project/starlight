<?php

/**
 * @file
 * Color module integration.
 */

$info = array(
  // Available colours and colour labels used in theme.
  // Field names should be lowercase only
  // (no underscores - they mess up the palette switcher js)
  'fields' => array(
    'base' => t('Page background colour'),
    'text' => t('Text colour'),
    'link' => t('Link color'),
    'headerfooterbg' => t('Header and Footer background'),
    'headerfooterfg' => t('Header and Footer foreground'),
    'menubg' => t('Main colour background'),
    'menufg' => t('Main colour foreground'),
    'highlightregionbg' => t('Highlight Region background'),
    'highlightregionfg' => t('Highlight Region foreground'),
    'highlightregionlinks' => t('Highlight Region links & buttons'),
    'accentbg' => t('Accent colour background'),
    'accentfg' => t('Accent colour forground'),
    'sidebarbg' => t('Sidebar Background'),
    'upperfooterbg' => t('Upper Footer background'),
    'upperfooterfg' => t('Upper Footer foreground'),
  ),
  // Pre-defined color schemes.
  'schemes' => array(
    'default' => array(
      'title' => t('Starlight'),
      'colors' => array(
        'base' => '#fff888',
        'text' => '#323232',
        'link' => '#3d599c',
        'headerfooterbg' => '#fff',
        'headerfooterfg' => '#303030',
        'menubg' => '#0d1271',
        'menufg' => '#ffffff',
        'highlightregionbg' => '#2d276d',
        'highlightregionfg' => '#efefef',
        'highlightregionlinks' => '#ffdf5d',
        'accentbg' => '#e3c446',
        'accentfg' => '#313131',
        'sidebarbg' => '#e4e4e4',
        'upperfooterbg' => '#bdbdbd',
        'upperfooterfg' => '#222222',
      ),
    ),
    'test' => array(
      'title' => t('DarkLight'),
      'colors' => array(
        'base' => '#2b2a32',
        'text' => '#323232',
        'link' => '#1b6dde',
        'headerfooterbg' => '#2b2a32',
        'headerfooterfg' => '#ffffff',
        'menubg' => '#1b6dde',
        'menufg' => '#ffffff',
        'highlightregionbg' => '#c6c5ce',
        'highlightregionfg' => '#2b2a32',
        'highlightregionlinks' => '#1b6dde',
        'accentbg' => '#e3c446',
        'accentfg' => '#313131',
        'sidebarbg' => '#e4e4e4',
        'upperfooterbg' => '#48474d',
        'upperfooterfg' => '#ffffff',
      ),
    ),
  ),

  // CSS files (excluding @import) to rewrite with new colour scheme.
  'css' => array(
    'components/global/css/colour.css',
  ),

  // Files to copy.
  'copy' => array(
    'logo.svg',
  ),

 // Gradient definitions.
  'gradients' => array(),

  // Preview files.
  'preview_library' => 'starlight/color-preview',
  'preview_html' => 'color/preview.html',

  // Attachments.
  '#attached' => [
    'drupalSettings' => [
      'color' => [
        // Put the logo path into JavaScript for the live preview.
        'logo' => theme_get_setting('logo.url', 'starlight'),
      ],
    ],
  ],


);
