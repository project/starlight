/**
 * @file
 * Preview for the Starlight theme.
 */
(function ($, Drupal, drupalSettings) {

  "use strict";

  Drupal.color = {
    logoChanged: false,
    callback: function (context, settings, form, farb, height, width) {
      // Change the logo to be the real one.
      if ((!this.logoChanged) && (drupalSettings.color.logo.indexOf('starlight') == 0)) {
        $('.colour-preview .logo img').attr('src', logo_path);
        $('.colour-preview .logo-text').hide();
        this.logoChanged = true;
      }

      // Remove the logo if the setting is toggled off.
      if (drupalSettings.color.logo === null) {
        $('.header').remove('.logo img');
        $('.colour-preview .logo-text').show();
      }

      // console.log(form);
      var $colorPreview = form.find('.colour-preview');
      // console.log($colorPreview);
      var $colorPalette = form.find('.js-color-palette');
      // console.log($colorPalette);

      // Text colour
      $colorPreview.find('.content-wrap').css('color', $colorPalette.find('input[name="palette[text]"]').val());
      // Lnk colour
      $colorPreview.find('.content-wrap a').css('color', $colorPalette.find('input[name="palette[link]"]').val());
      // Header foreground
      $colorPreview.find('.header, .lower-footer').css('color', $colorPalette.find('input[name="palette[headerfooterfg]"]').val());
      // Header background
      $colorPreview.find('.header, .lower-footer').css('background-color', $colorPalette.find('input[name="palette[headerfooterbg]"]').val());
      // Menu foreground
      $colorPreview.find('.main-menu, .main-menu a').css('color', $colorPalette.find('input[name="palette[menufg]"]').val());
      // Menu background
      $colorPreview.find('.main-menu').css('background-color', $colorPalette.find('input[name="palette[menubg]"]').val());
      // Menu background as foreground
      $colorPreview.find('h1.preview-site-name span').css('color', $colorPalette.find('input[name="palette[menubg]"]').val());
      // Highlight foreground
      $colorPreview.find('.highlight').css('color', $colorPalette.find('input[name="palette[highlightregionfg]"]').val());
      // Highlight background
      $colorPreview.find('.highlight').css('background-color', $colorPalette.find('input[name="palette[highlightregionbg]"]').val());
      // Highlight links
      $colorPreview.find('.highlight a, .highlight h2').css('color', $colorPalette.find('input[name="palette[highlightregionlinks]"]').val());
      // Accent foreground
      $colorPreview.find('.button, .form-submit').css('color', $colorPalette.find('input[name="palette[accentfg]"]').val());
      // Accent background
      $colorPreview.find('.button, .form-submit').css('background-color', $colorPalette.find('input[name="palette[accentbg]"]').val());
      // Accent background as foreground
      $colorPreview.find('h1.preview-site-name').css('color', $colorPalette.find('input[name="palette[accentbg]"]').val());
      // Sidebar background
      $colorPreview.find('.sidebar').css('background-color', $colorPalette.find('input[name="palette[sidebarbg]"]').val());
      // Upper Footer foreground
      $colorPreview.find('.upper-footer, .upper-footer a').css('color', $colorPalette.find('input[name="palette[upperfooterfg]"]').val());
      // Upper Footer background
      $colorPreview.find('.upper-footer').css('background-color', $colorPalette.find('input[name="palette[upperfooterbg]"]').val());

    }
  };
})(jQuery, Drupal, drupalSettings);
