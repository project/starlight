<?php
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Form\ThemeSettingsForm;
use Drupal\file\Entity\File;
use Drupal\Core\Url;

/**
 * Implements hook_form_system_theme_settings_alter().
 *
 * Custom theme settings
 */

 function starlight_form_system_theme_settings_alter(&$form,&$form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }
  $form['theme_settings']['toggle_comment_subject'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Show comment subject'),
    '#default_value' => theme_get_setting('toggle_comment_subject'),
  );
  $form['theme_settings']['submitted_show_author'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Author name in "Submitted by" line'),
    '#default_value' => theme_get_setting('submitted_show_author'),
  );
  $form['theme_settings']['submitted_link_author'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Link post author name to user profile in "Submitted by" line'),
    '#default_value' => theme_get_setting('submitted_link_author'),
  );

  // Overall design settings
  $form['design-settings'] = array(
    '#type' => 'details',
    '#title' => t('Theme design settings'),
    '#open' => FALSE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
  );
  $form['design-settings']['font'] = array(
    '#type' => 'select',
    '#title' => t('Font'),
    '#options' => [
      'default' => 'Default',
      'lato' => 'Lato',
      'muli' => 'Muli',
      'noto_sans' => 'Noto Sans',
      'open_sans' => 'Open Sans',
      'oxygen' =>  'Oxygen',
      'pt_sans' =>  'PT Sans',
      'ubuntu' => 'Ubuntu',
    ],
    '#default_value' => theme_get_setting('font'),
  );
  // Header settings
  $form['header'] = array(
    '#type' => 'details',
    '#title' => t('Header layout settings'),
    '#open' => FALSE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
  );
  $form['header']['header_horiz'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use Horizontal Layout'),
    '#default_value' => theme_get_setting('header_horiz'),
    '#description'   => t("Use horizontal layout for the top header region (all blocks display in one row)"),
  );
  $form['header']['header_sticky'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Make header sticky on scroll'),
    '#default_value' => theme_get_setting('header_sticky'),
    '#description'   => t("On larger screens, make the header stick to the top of the window as you scroll down. Works with the mobile menu breakpoint setting, below."),
  );
  $form['header']['search_collapse'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Collapse search box to icon on larger screens'),
    '#default_value' => theme_get_setting('search_collapse'),
    '#description'   => t("Display search block as an icon that toggles the form display. This will happen by default on mobile."),
  );
  $form['header']['light_on_dark'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use "light on dark" Header scheme'),
    '#default_value' => theme_get_setting('search_collapse'),
    '#description'   => t("Improves search box theming and menu hovers for dark backgrounds with light text"),
  );
  // Highlight region settings
  $form['highlight_settings'] = array(
    '#type' => 'details',
    '#title' => t('Highlight Region settings'),
    '#open' => FALSE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
  );
  $form['highlight_settings']['highlight_bg_file'] = array(
    '#type' => 'textfield',
    '#title' => t('URL of the highlight region background image'),
    '#default_value' => theme_get_setting('highlight_bg_file'),
    '#description' => t('Enter a path in the form (/sites/default/files/your-background.jpg).'),
    '#size' => 40,
    '#maxlength' => 120,
  );
  $form['highlight_settings']['highlight_bg_img'] = array(
    '#type' => 'file',
    '#title' => t('Highlight region background image'),
    '#size' => 40,
    '#attributes' => array('enctype' => 'multipart/form-data'),
    '#description' => t('Uploads limited to .png .gif .jpg .jpeg .apng .svg extensions'),
    '#element_validate' => array('starlight_header_bg_validate'),
  );
  $form['highlight_settings']['highlight_bg_layout'] = array(
    '#type'          => 'select',
    '#title'         => t('Highlight region background image layout'),
    '#default_value' => theme_get_setting('highlight_bg_image_layout'),
    '#options' => [
      'tile' => 'Tile',
      'cover' => 'Cover',
      'none' => 'None',
    ],
    '#description'   => t("Set background properties for the highlight region background image. 'Tile' will repeat the image continuously (best for repeating patterns). 'Cover' will force the image to cover the entire header area regardless of screen size, while maintaining aspect ratio. Image styles will automatically be applied to scale the image appropriately on smaller screens. 'None' will keep the image at the size uploaded here with no repeating."),
  );
  // Menu region settings
  $form['menu_settings'] = array(
    '#type' => 'details',
    '#title' => t('menu settings'),
    '#open' => FALSE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
  );
  // Mobile menu behaviour option
  $form['menu_settings']['mobile_menu'] = array(
    '#type' => 'select',
    '#title' => t('Mobile menu display'),
    '#options' => [
      'inline' => 'Inline',
      'standard' => 'Standard slide toggle',
      'push_top' => 'Push down from top',
      'push_left' => 'Push out from left',
      'push_right' => 'Push out from right',
    ],
    '#description' => '"Inline" will display the links inline with no slide down behaviour. This will only work well if you do not have secondary menu levels.',
    '#default_value' => theme_get_setting('mobile_menu'),
  );
  $form['menu_settings']['mobile_menu_breakpoint'] = array(
    '#type' => 'number',
    '#title' => 'Mobile menu breakpoint',
    '#default_value' => theme_get_setting('mobile_menu_breakpoint'),
    '#field_suffix'  => '(less than)',
    '#description' => 'Pixel width to trigger the mobile menu. Viewports smaller than this will get the mobile menu layout.'
  );
  /* Menu settings */
  $form['menu_settings']['menu_align'] = array(
    '#type' => 'select',
    '#title' => t('Main menu alignment in main menu region'),
    '#options' => [
      'left' => 'Left',
      'right' => 'Right',
      'center' => 'center',
      'justify' => 'Justify',
    ],
    '#default_value' => theme_get_setting('menu_align'),
  );
}

/**
 * Check and save the uploaded header background image
 */
function starlight_header_bg_validate($element, FormStateInterface $form_state) {
  global $base_url;

  $validators = array('file_validate_extensions' => array('png gif jpg jpeg apng svg'));
  $file = file_save_upload('highlight_bg_img', $validators, "public://", NULL, FILE_EXISTS_REPLACE);

  if (!empty($file)) {
    // change file's status from temporary to permanent and update file database
    if ((is_object($file[0]) == 1)) {
      $file[0]->status = FILE_STATUS_PERMANENT;
      $file[0]->save();
      $uri = $file[0]->getFileUri();
      $file_url = file_create_url($uri);
      $file_url = str_ireplace($base_url, '', $file_url);
      $form_state->setValue('highlight_bg_file', $file_url);
    }
 }
}
